# docker-megasync-tftp

Probando la integración de docker con volumenes remotos (tftp y megasync)

## Plugin de Docker

Este plugin debe ser instalado en el servidor de Docker que se requiera conectar un volumen remoto.

### Instalación del plugin

docker plugin install --grant-all-permissions vieux/sshfs

### Cración del Volumen remoto

docker volume create --driver vieux/sshfs -o sshcmd={user}@{server}:{routeToShareDir} -o password={testPassword} -o allow_other -o nonempty {volumeName}

### Data Persistente

Debe crearse un directorio para que el voluen Persistente apunte a /megasync/.local/
